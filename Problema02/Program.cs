﻿using System;

namespace Problema02
{
    class Program
    {
        static void Main(string[] args)
        {
            int valor;
            int indice = 1;
            //int[] numerosEntrada = { 2, 1, 4, 5};
            int[] numerosEntrada = new int[indice];
            do
            {
                Array.Resize(ref numerosEntrada, indice);
                Console.WriteLine("Ingrese elemento del arreglo: ");
                var input = Convert.ToString(Console.ReadLine());
                var esNumero = int.TryParse(input, out valor);
                if (int.TryParse(input, out valor))
                {
                    numerosEntrada[indice - 1] = Convert.ToInt32(input.ToString());
                    indice += 1;
                }

                Console.WriteLine(esNumero ? "Desea seguir ingresando elementos al arreglo <S/N>" : "No es un número, Desea continuar <S/N>:");
            } while (Convert.ToString(Console.ReadLine()).ToUpper() == "S");
            (new OrderRange()).Build(numerosEntrada); 
            Console.ReadKey();
        }
    }
}
