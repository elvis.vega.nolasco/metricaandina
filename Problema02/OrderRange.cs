﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Problema02
{
    public class OrderRange
    {
        public void Build(int[] numerosEntrada)
        {
            LinkedList<int> numeroEvaluar = new LinkedList<int>(numerosEntrada);
            LinkedList<int> numerosPar = new LinkedList<int>();
            LinkedList<int> numerosInPar = new LinkedList<int>();
            var intNumAnt = 0;
            var numeroMenor = 0;
            foreach (int numero in numeroEvaluar)
            {
                if (numero < intNumAnt || intNumAnt <= 0)
                    numeroMenor = numero;
                
                if ((numero % 2) == 0)
                    numerosPar.AddLast(numero);
                else
                    numerosInPar.AddLast(numero);
                intNumAnt = numero;
            }
            var strnumpar = string.Join(", ", numerosPar);
            var strnumerosInPar = string.Join(", ", numerosInPar);

            if (strnumpar.IndexOf((char)numeroMenor) != -1)
                Console.WriteLine("Salida: [ " + strnumpar + " ], [ " + strnumerosInPar + "]");
            else 
                Console.WriteLine("Salida: [ " + strnumerosInPar + " ], [ " + strnumpar + "]");
        }
    }
}
