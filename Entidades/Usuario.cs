﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class Usuario
    {
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string strUser;

        public string User
        {
            get { return strUser ; }
            set { strUser = value; }
        }

        private string strPasword;

        public string Password
        {
            get { return strPasword; }
            set { strPasword = value; }
        }

        private DateTime  datFechaRegistro;

        public DateTime FechaRegistro
        {
            get { return datFechaRegistro; }
            set { datFechaRegistro = value; }
        }

        private int intEstado;
        public int Estado
        {
            get { return intEstado; }
            set { intEstado = value; }
        }

    }
}
