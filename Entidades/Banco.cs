﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class Banco : GeneralEntities
    {

        private List<Sucursal> objSucursal;

        public List<Sucursal> lstSucursal
        {
            get { return objSucursal; }
            set { objSucursal = value; }
        }

        private List<Sucursal> lstSucursalesdisponibles;

        public List<Sucursal> Sucursalesdisponibles
        {
            get { return lstSucursalesdisponibles; }
            set { lstSucursalesdisponibles = value; }
        }
        private int intIdSucursal;

        public int IdSucursal
        {
            get { return intIdSucursal; }
            set { intIdSucursal = value; }
        }
      
    }
}
