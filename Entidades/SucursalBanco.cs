﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class SucursalBanco
    {
        private int intIdBanco;

        public int IdBanco
        {
            get { return intIdBanco; }
            set { intIdBanco = value; }
        }

        private int intIdSucursal;

        public int IdSucursal
        {
            get { return intIdSucursal; }
            set { intIdSucursal = value; }
        }
        private string strIdBancoSucursal;

        public string IdBancoSucursal
        {
            get { return strIdBancoSucursal; }
            set { strIdBancoSucursal = value; }
        }

        private string strDescripcion;

        public string Descripcion
        {
            get { return strDescripcion; }
            set { strDescripcion = value; }
        }


    }
}
