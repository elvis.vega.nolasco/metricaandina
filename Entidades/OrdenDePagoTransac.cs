﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class OrdenDePagoTransac: SucursalBanco
    {
        private OrdenDePago objOrden;

        public OrdenDePago Orden
        {
            get { return objOrden; }
            set { objOrden = value; }
        }
    }
}
