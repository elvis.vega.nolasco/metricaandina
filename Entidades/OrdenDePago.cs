﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entidades
{
    public class OrdenDePago
    {
        private int intIdOrden;

        public int IdOrden
        {
            get { return intIdOrden; }
            set { intIdOrden = value; }
        }

        private decimal decMonto;

        [RegularExpression("(.*[1-9].*)|(.*[.].*[1-9].*)", ErrorMessage ="Ingrese un monto mayor")]
        public decimal Monto
        {
            get { return decMonto; }
            set { decMonto = value; }
        }

        private string  strMoneda;

        public string  Moneda
        {
            get { return strMoneda; }
            set { strMoneda = value; }
        }
        private string strEstado;

        public string Estado
        {
            get { return strEstado; }
            set { strEstado = value; }
        }

        private DateTime  datFechaPago;

        public DateTime  fechaPago
        {
            get { return datFechaPago; }
            set { datFechaPago = value; }
        }

        private string strDetallear;

        public string Detalle
        {
            get { return strDetallear; }
            set { strDetallear = value; }
        }


    }
}
