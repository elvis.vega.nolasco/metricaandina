﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Entidades
{
    public class GeneralEntities
    {
        private int intId;

        public int Id
        {
            get { return intId; }
            set { intId = value; }
        }
      
        private string strNombre;

        [Required(ErrorMessage = "Ingrese, Campo obligatorio*")]
        [StringLength(10)]
        public string Nombre
        {
            get { return strNombre; }
            set { strNombre = value; }
        }


        private string strDireccion;

        [Required(ErrorMessage = "Ingrese, Campo obligatorio*")]
        [StringLength(20)]
        public string Direccion
        {
            get { return strDireccion; }
            set { strDireccion = value; }
        }

        private DateTime datFechaRegistro;

        public DateTime FechaRegistro
        {
            get { return datFechaRegistro; }
            set { datFechaRegistro = value; }
        }

    }
}
