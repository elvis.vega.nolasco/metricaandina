﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class Roles
    {
        private int intId;

        public int Id
        {
            get { return intId; }
            set { intId = value; }
        }

        private string strRol;

        public string Rol
        {
            get { return strRol; }
            set { strRol = value; }
        }

        private DateTime  datFechaRegistro;

        public DateTime FechaRegistro
        {
            get { return datFechaRegistro; }
            set { datFechaRegistro = value; }
        }

    }
}
