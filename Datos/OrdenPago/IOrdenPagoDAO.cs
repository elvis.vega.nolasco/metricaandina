﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.OrdenPago
{
   public  interface IOrdenPagoDAO
    {
        List<Entidades.OrdenDePagoTransac> _OrdenDePagoTransac();
        List<Entidades.OrdenDePago> _getListaOrdenDePago();
        List<Entidades.OrdenDePago> _getListaOrdenDePagoPendientes();
        Entidades.OrdenDePago _getOrdenDePagoxId(int id);
        void _editOrdenDePago(Entidades.OrdenDePago objOrdenDePago);
        void _eliminarOrdenDePago(int id);
        void _guardarOrdenDePago(Entidades.OrdenDePago objOrdenDePago);
        List<Entidades.SucursalBanco> _getSucursalBancoAdociado();
        void _guardarOrdenDePagoTransaccion(int IdOrden, int idBanco, int idSucursal);

        Task<List<Entidades.OrdenDePagoTransac>> _getOrdenDePagoxSucursal(string tipoMoneda);

    }
}
