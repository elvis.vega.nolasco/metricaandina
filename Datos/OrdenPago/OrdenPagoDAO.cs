﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.OrdenPago
{
    public class OrdenPagoDAO : IOrdenPagoDAO
    {
        public IConfiguration Configuration { get; }
        public OrdenPagoDAO(IConfiguration configuration)
        {
            Configuration = configuration;
            if (!BaseDatos.CheckDatabaseExists("db_banco"))
            {
                var bd = new BaseDatos();
                bd.crearbasededatos();
            }
        }
        public List<Entidades.SucursalBanco> _getSucursalBancoAdociado(){
            List<Entidades.SucursalBanco> lstSucursalBanco = new List<Entidades.SucursalBanco>();
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConeccion))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("get_Sucursales_banco", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var OrdenDePago = new Entidades.SucursalBanco();
                            OrdenDePago.IdBanco = (int)rdr["id_banco"];
                            OrdenDePago.IdSucursal = (int)rdr["id_sucursal"];
                            OrdenDePago.Descripcion = (string)rdr["bancoSucursal"];
                            OrdenDePago.IdBancoSucursal = OrdenDePago.IdBanco + "-" + OrdenDePago.IdSucursal;
                            lstSucursalBanco.Add(OrdenDePago);
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return lstSucursalBanco;
        }
       
        public List<Entidades.OrdenDePagoTransac> _OrdenDePagoTransac()
        {
            List<Entidades.OrdenDePagoTransac> lstOrdenDePago = new List<Entidades.OrdenDePagoTransac>();
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConeccion))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("listar_OrdenDePagotransac", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var OrdenDePago = new Entidades.OrdenDePagoTransac();
                            OrdenDePago.Orden = new Entidades.OrdenDePago() { IdOrden = (int)rdr["id_ordenpago"] , Monto= (decimal)rdr["monto"], Moneda = (string)rdr["moneda"] };
                            OrdenDePago.IdBanco = (int)rdr["id_banco"];
                            OrdenDePago.IdSucursal = (int)rdr["id_sucursal"];
                            OrdenDePago.Descripcion = (string)rdr["descripcion"]; 
                            lstOrdenDePago.Add(OrdenDePago);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return lstOrdenDePago;
        }
        public List<Entidades.OrdenDePago> _getListaOrdenDePago()
        {
            List<Entidades.OrdenDePago> lstOrdenDePago = new List<Entidades.OrdenDePago>();
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConeccion))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("listar_OrdenDePago", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var OrdenDePago = new Entidades.OrdenDePago();
                            OrdenDePago.IdOrden = (int)rdr["id_ordenpago"];
                            OrdenDePago.Moneda  = (string)rdr["moneda"];
                            OrdenDePago.Monto  = (decimal)rdr["monto"];
                            OrdenDePago.Estado = (string)rdr["estado"];
                            OrdenDePago.fechaPago  = (DateTime)rdr["fecha_registro"];
                            lstOrdenDePago.Add(OrdenDePago);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return lstOrdenDePago;
        }
        public List<Entidades.OrdenDePago> _getListaOrdenDePagoPendientes()
        {
            List<Entidades.OrdenDePago> lstOrdenDePago = new List<Entidades.OrdenDePago>();
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConeccion))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("listar_OrdenDePagoPending", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var OrdenDePago = new Entidades.OrdenDePago();
                            OrdenDePago.IdOrden = (int)rdr["id_ordenpago"];
                            OrdenDePago.Moneda = (string)rdr["moneda"];
                            OrdenDePago.Monto = (decimal)rdr["monto"];
                            OrdenDePago.Estado = (string)rdr["estado"];
                            OrdenDePago.fechaPago = (DateTime)rdr["fecha_registro"];
                            OrdenDePago.Detalle = "Orden: "+ OrdenDePago.IdOrden + " - Total: " + OrdenDePago.Monto + " " + OrdenDePago.Moneda;
                            lstOrdenDePago.Add(OrdenDePago);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return lstOrdenDePago;
        }
        public async Task<List<Entidades.OrdenDePagoTransac>> _getOrdenDePagoxSucursal(string tipoMoneda)
        {
            List<Entidades.OrdenDePagoTransac> lstOrdenDePago = new List<Entidades.OrdenDePagoTransac>();
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");

            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("listar_OrdenDePagoSucursal", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_in_moneda", tipoMoneda));
                using (SqlDataReader rdr = await cmd.ExecuteReaderAsync())
                {
                    while (rdr.Read())
                    {
                        var OrdenDePago = new Entidades.OrdenDePagoTransac();
                        OrdenDePago.Orden = new Entidades.OrdenDePago() { IdOrden = (int)rdr["id_ordenpago"], Moneda = (string)rdr["moneda"], Monto = (decimal)rdr["monto"] }; 
                        OrdenDePago.Descripcion = (string)rdr["descripcion"];
                        lstOrdenDePago.Add(OrdenDePago);
                    }
                }
            }
            return lstOrdenDePago;
        }

        public Entidades.OrdenDePago _getOrdenDePagoxId(int id)
        {
            Entidades.OrdenDePago OrdenDePago = null;
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");

            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("get_OrdenDePagoxId", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_OrdenDePago", id));
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        OrdenDePago = new Entidades.OrdenDePago();
                        OrdenDePago.IdOrden = (int)rdr["id_ordenpago"];
                        OrdenDePago.Moneda = (string)rdr["moneda"];
                        OrdenDePago.Monto = (decimal)rdr["monto"];
                        OrdenDePago.Estado = (string)rdr["estado"];
                        OrdenDePago.fechaPago = (DateTime)rdr["fecha_registro"];
                    }
                }
            }
            return OrdenDePago;
        }

        public void _editOrdenDePago(Entidades.OrdenDePago OrdenDePago)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");

            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("update_OrdenesPago", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_ordenpago", OrdenDePago.IdOrden));
                cmd.Parameters.Add(new SqlParameter("@p_monto", OrdenDePago.Monto));
                cmd.Parameters.Add(new SqlParameter("@p_moneda", OrdenDePago.Moneda));
                cmd.Parameters.Add(new SqlParameter("@p_estado", OrdenDePago.Estado));
                cmd.ExecuteNonQuery();
            }
        }
        public void _guardarOrdenDePagoTransaccion(int IdOrden, int idBanco, int idSucursal)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");

            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("insert_OrdenesPagoTransac", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_orden", IdOrden));
                cmd.Parameters.Add(new SqlParameter("@p_id_banco", idBanco));
                cmd.Parameters.Add(new SqlParameter("@p_id_sucursal", idSucursal));
                cmd.ExecuteNonQuery();
            }
        }
        public void _guardarOrdenDePago(Entidades.OrdenDePago OrdenDePago)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");

            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("insert_OrdenesPago", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_monto", OrdenDePago.Monto));
                cmd.Parameters.Add(new SqlParameter("@p_moneda", OrdenDePago.Moneda));
                cmd.Parameters.Add(new SqlParameter("@p_estado", OrdenDePago.Estado));
                cmd.ExecuteNonQuery();
            }
        }
        public void _eliminarOrdenDePago(int id)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("delete_OrdenesPago", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_ordenpago", id));
                cmd.ExecuteNonQuery();
            }
        }
    }
}
