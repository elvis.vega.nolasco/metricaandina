﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos.Banco
{
    public interface IBancoDAO
    {
       List<Entidades.Banco>  _getListaBancos();
        Entidades.Banco _getBancoxId(int id);
        void _editBanco(Entidades.Banco objBanco);
        void _eliminarBanco(int id);
        void _guardarBanco(Entidades.Banco objBanco);
    }
}

