﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using System.Text;
namespace AccesoDatos.Banco
{
    public class BancoDAO : IBancoDAO
    {
        public IConfiguration Configuration { get; }
        public BancoDAO(IConfiguration configuration)
        {
            Configuration = configuration;
            if (!BaseDatos.CheckDatabaseExists("db_banco"))
            {
                var bd = new BaseDatos();
                bd.crearbasededatos();
            }
        }

        public List<Entidades.Banco> _getListaBancos()
        {
            List<Entidades.Banco> lstBancos = new List<Entidades.Banco>();
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConeccion))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("listar_Bancos", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var banco = new Entidades.Banco();
                            banco.Id = (int)rdr["id_banco"];
                            banco.Nombre = (string)rdr["nombre"];
                            banco.Direccion = (string)rdr["direccion"];
                            banco.FechaRegistro = (DateTime)rdr["fecha_registro"];
                            lstBancos.Add(banco);
                        }
                    }
                }
            }
            catch (Exception )
            {


            }
            return lstBancos;
        }
        public Entidades.Banco _getBancoxId(int id)
        {
            Entidades.Banco banco = null;
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");

            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("get_BancosxId", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_banco", id));
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        banco = new Entidades.Banco();
                        banco.Id = (int)rdr["id_banco"];
                        banco.Nombre = (string)rdr["nombre"];
                        banco.Direccion = (string)rdr["direccion"];
                        banco.FechaRegistro = (DateTime)rdr["fecha_registro"];
                    }
                }
            }
            return banco;
        }

        public void _editBanco(Entidades.Banco objBanco)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");

            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("update_banco", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_banco", objBanco.Id));
                cmd.Parameters.Add(new SqlParameter("@p_nombre", objBanco.Nombre));
                cmd.Parameters.Add(new SqlParameter("@p_direccion", objBanco.Direccion));
                cmd.ExecuteNonQuery();
            }
        }
        
        public void _guardarBanco(Entidades.Banco objBanco)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");

            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("insert_banco", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_nombre", objBanco.Nombre));
                cmd.Parameters.Add(new SqlParameter("@p_direccion", objBanco.Direccion));
                cmd.ExecuteNonQuery();
            }
        }
        public void _eliminarBanco(int id)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("delete_banco", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_banco", id));
                cmd.ExecuteNonQuery();
            }
        }
    }
}
     