﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccesoDatos.Sucursal
{
    public interface ISucursalDAO
    {
        List<Entidades.Sucursal> _getListaSucursal();
        Entidades.Sucursal _getSucursalxId(int id);
        void _editSucursal(Entidades.Sucursal objBanco);
        void _eliminarSucursal(int id);
        void _guardarSucursal(Entidades.Sucursal objBanco);
        List<Entidades.Sucursal> _getListaSucursalPorIdBanco(int idBanco);
        List<Entidades.Sucursal> _getListaSucursalDisponiblesParaBanco(int idBanco);

        void _asociarBancoSucursal(int idHotel, int idSucursal);
        void _EliminarSucursalDeBanco(int idBanco, int idSucursal);
    }
}
