﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace AccesoDatos.Sucursal
{
    public class SucursalDAO : ISucursalDAO
    {
        public IConfiguration Configuration { get; }
        public SucursalDAO(IConfiguration configuration)
        {
            Configuration = configuration;
            if (!BaseDatos.CheckDatabaseExists("db_banco"))
            {
                var bd = new BaseDatos();
                bd.crearbasededatos();
            }
        }
        public List<Entidades.Sucursal> _getListaSucursalDisponiblesParaBanco(int idBanco)
        {
            List<Entidades.Sucursal> lstSucursal = new List<Entidades.Sucursal>();
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConeccion))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("listar_SucursalDispoPorIdBanco", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@p_id_banco", idBanco));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var sucursal = new Entidades.Sucursal();
                            sucursal.Id = (int)rdr["id_sucursal"];
                            sucursal.Nombre = (string)rdr["nombre"];
                            sucursal.Direccion = (string)rdr["direccion"];
                            sucursal.FechaRegistro = (DateTime)rdr["fecha_registro"];
                            lstSucursal.Add(sucursal);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return lstSucursal;
        }
        public List<Entidades.Sucursal> _getListaSucursalPorIdBanco(int idBanco)
        {
            List<Entidades.Sucursal> lstSucursal = new List<Entidades.Sucursal>();
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConeccion))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("listar_SucursalPorIdBanco", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@p_id_banco", idBanco));
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var sucursal = new Entidades.Sucursal();
                            sucursal.Id = (int)rdr["id_sucursal"];
                            sucursal.Nombre = (string)rdr["nombre"];
                            sucursal.Direccion = (string)rdr["direccion"];
                            sucursal.FechaRegistro = (DateTime)rdr["fecha_registro"];
                            lstSucursal.Add(sucursal);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            return lstSucursal;
        }
        public List<Entidades.Sucursal> _getListaSucursal()
        {
            List<Entidades.Sucursal> lstSucursal = new List<Entidades.Sucursal>();
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            try
            {
                using (SqlConnection conn = new SqlConnection(cadenaConeccion))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("listar_Sucursal", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            var sucursal = new Entidades.Sucursal();
                            sucursal.Id = (int)rdr["id_sucursal"];
                            sucursal.Nombre = (string)rdr["nombre"];
                            sucursal.Direccion = (string)rdr["direccion"];
                            sucursal.FechaRegistro = (DateTime)rdr["fecha_registro"];
                            lstSucursal.Add(sucursal);
                        }
                    }
                }
            }
            catch (Exception )
            {


            }
            return lstSucursal;
        }
        public Entidades.Sucursal _getSucursalxId(int id)
        {
            Entidades.Sucursal sucursal = null;
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");

            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("get_SucursalxId", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_sucursal", id));
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        sucursal = new Entidades.Sucursal();
                        sucursal.Id = (int)rdr["id_Sucursal"];
                        sucursal.Nombre = (string)rdr["nombre"];
                        sucursal.Direccion = (string)rdr["direccion"];
                        sucursal.FechaRegistro = (DateTime)rdr["fecha_registro"];
                    }
                }
            }
            return sucursal;
        }

        public void _editSucursal(Entidades.Sucursal objSucursal)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("update_sucursal", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_sucursal", objSucursal.Id));
                cmd.Parameters.Add(new SqlParameter("@p_nombre", objSucursal.Nombre));
                cmd.Parameters.Add(new SqlParameter("@p_direccion", objSucursal.Direccion));
                cmd.ExecuteNonQuery();
            }
        }
        public void _asociarBancoSucursal( int idHotel, int idSucursal)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("insert_BancoSucursal", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_banco", idHotel));
                cmd.Parameters.Add(new SqlParameter("@p_id_sucursal", idSucursal));
                cmd.ExecuteNonQuery();
            }
        }
        public void _guardarSucursal(Entidades.Sucursal objSucursal)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("insert_sucursal", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_nombre", objSucursal.Nombre));
                cmd.Parameters.Add(new SqlParameter("@p_direccion", objSucursal.Direccion));
                cmd.ExecuteNonQuery();
            }
        }
        public void _eliminarSucursal(int id)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("delete_sucursal", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_sucursal", id));
                cmd.ExecuteNonQuery();
            }
        }
        public void _EliminarSucursalDeBanco(int idBanco, int idSucursal)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("delete_sucursaldebanco", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id_banco", idBanco));
                cmd.Parameters.Add(new SqlParameter("@p_id_sucursal", idSucursal));
                cmd.ExecuteNonQuery();
            }
        }
    }
}
