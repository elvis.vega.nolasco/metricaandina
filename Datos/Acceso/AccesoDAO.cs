﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace AccesoDatos.Acceso
{
    public class AccesoDAO : IAccesoDAO
    {
        public IConfiguration Configuration { get; }
        public AccesoDAO(IConfiguration configuration)
        {
            Configuration = configuration;
          
            if (!BaseDatos.CheckDatabaseExists("db_banco"))
            {
                var bd = new BaseDatos();
                bd.crearbasededatos();
            }
            
        }
        public bool _ValidaUsuario(string user, string pasword, string Rol)
        {
            var cadenaConeccion = Configuration.GetConnectionString("DefaultConnection");

            using (SqlConnection conn = new SqlConnection(cadenaConeccion))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("validarCuenta", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_user", user));
                cmd.Parameters.Add(new SqlParameter("@p_pasword", pasword));
                cmd.Parameters.Add(new SqlParameter("@p_rol", Rol));
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
