﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace AccesoDatos.SucursalBanco
{
    public class SucursalBancoDAO : ISucursalBancoDAO
    {
        public IConfiguration Configuration { get; }
        public SucursalBancoDAO(IConfiguration configuration)
        {
            Configuration = configuration;
            if (!BaseDatos.CheckDatabaseExists("db_banco"))
            {
                var bd = new BaseDatos();
                bd.crearbasededatos();
            }
        }
    }
}
