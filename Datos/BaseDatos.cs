﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace AccesoDatos
{
    public class BaseDatos
    {
       
        public void crearbasededatos()
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            Assembly asm = Assembly.GetExecutingAssembly();
            string pathx = System.IO.Path.GetDirectoryName(asm.Location);
            try
            {
                string ruta = pathx + "\\db_banco.bak";
                string sBackup = "RESTORE DATABASE " + "db_banco" +
                                 " FROM DISK = '" + ruta + "'" +
                                 " WITH REPLACE";

                SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
                csb.DataSource = ".";
                csb.InitialCatalog = "master";
                csb.IntegratedSecurity = true;

                using (SqlConnection con = new SqlConnection(csb.ConnectionString))
                {
                    con.Open();
                    SqlCommand cmdBackUp = new SqlCommand(sBackup, con);
                    cmdBackUp.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
              
            }
        }

        public static bool CheckDatabaseExists(string dataBase)
        {
            

            string conStr = "Data Source=.;Initial Catalog=master;Integrated Security=True";
            string cmdText = "SELECT * FROM master.dbo.sysdatabases WHERE name ='" + dataBase + "'";
            bool isExist = false;
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(cmdText, con))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        isExist = reader.HasRows;
                    }
                }
                con.Close();
            }
            return isExist;
        }
    }
}
