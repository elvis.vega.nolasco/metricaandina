﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Problema03
{
   public class MoneyParts
    {
       
        public MoneyParts() {
            string.Format(CultureInfo.InvariantCulture, "0.00");
        }
        public void Build(double numeroEntrada)
        {

            double[] set = { 0.05 , 0.1, 0.2, 0.5, 1, 2, 5, 10, 20, 50, 100, 200 };
            foreach (string s in GetCombinations(set, numeroEntrada, ""))
            {
                Console.WriteLine(s);
            }
        }
        public static IEnumerable<string> GetCombinations(double[] set, double sum, string values)
        {
            for (int i = 0; i < set.Length; i++)
            {
                double left = sum - set[i];
                string vals = set[i].ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) + "," + values;
                if (left == 0)
                {
                    yield return vals;
                }
                else
                {
                    double[] possible = set.Where(n => n <= sum).ToArray();
                    if (possible.Length > 0)
                    {
                        foreach (string s in GetCombinations(possible, left, vals))
                        {
                            yield return s;
                        }
                    }
                }
            }
        }
    }
}
