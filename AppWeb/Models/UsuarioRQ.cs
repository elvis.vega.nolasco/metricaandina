﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AppWeb.Models
{
    public class UsuarioRQ //: IValidatableObject
    {
        [Required(ErrorMessage ="Debe Ingresar un usuario")]
        [StringLength(20)]
        public string Usuario { get; set; }
        
        [Required(ErrorMessage = "Debe Ingresar un password")]
        [StringLength(10)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Debe seleccionar un Rol")]
        public string Rol { get; set; }

        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
          
        //}
    }
}
