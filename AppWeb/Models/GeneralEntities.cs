﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AppWeb.Models
{
    public class GeneralEntities
    {

        public int Id;

        [Required(ErrorMessage = "Debe Ingresar un nombre")]
        [StringLength(30)]
        public string Nombre;

        [Required(ErrorMessage = "Debe Ingresar una dirección")]
        [StringLength(30)]
        public string Direccion;
      
        public DateTime FechaRegistro;

    }
}
