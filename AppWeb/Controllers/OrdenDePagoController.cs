﻿using AppWeb.Attributes;
using Bussines.OrdenPago;
using Entidades;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppWeb.Controllers
{
    [Autorizar("Operador2", "Administrador")]
    public class OrdenDePagoController : Controller
    {
        private readonly IOrdenDePagoBLL _OrdenDePagoBLL;

        public OrdenDePagoController(IOrdenDePagoBLL OrdenDePagoBLL)
        {
            this._OrdenDePagoBLL = OrdenDePagoBLL;
        }
        public IActionResult Index()
        {
            return View("~/Views/OrdenDePago/listarOrdenDePago.cshtml", _OrdenDePagoBLL.getListaOrdenDePago());
        }
        //[HttpGet("/Banco/Editar/{id}")]
        public IActionResult FormEditar(int id)
        {
            Entidades.OrdenDePago objOrdenDePago;
            if (id > 0)
            {
                objOrdenDePago = _OrdenDePagoBLL.getOrdenDePagoxId(id);
            }
            else
            {
                objOrdenDePago = new Entidades.OrdenDePago() { IdOrden = id };
            }

            return View("~/Views/OrdenDePago/_form_OrdenDePago.cshtml", objOrdenDePago);
        }
        public IActionResult Editar(Entidades.OrdenDePago objOrdenDePago)
        {
            if (!ModelState.IsValid)
            {
                return View("~/Views/OrdenDePago/_form_OrdenDePago.cshtml", objOrdenDePago);
            }
            if (objOrdenDePago.IdOrden == 0)
            {
                _OrdenDePagoBLL.guardarOrdenDePago(objOrdenDePago);
            }
            else
            {
                _OrdenDePagoBLL.editOrdenDePago(objOrdenDePago);
            }

            return RedirectToAction("Index", "OrdenDePago");
        }
        public IActionResult Eliminar(int id)
        {
            _OrdenDePagoBLL.eliminarOrdenDePago(id);
            return RedirectToAction("Index", "OrdenDePago");
        }
        public IActionResult formPagarOrdenPagoView()
        {
            return View("~/Views/OrdenDePago/listartransacciones.cshtml", _OrdenDePagoBLL.OrdenDePagoTransac());
        }
        public IActionResult formPagarOrdenPago()
        {
            ViewBag.OrdenesPagoPendientes = _OrdenDePagoBLL.getListaOrdenDePagoPendientes();
            ViewBag.SucusalBancos = _OrdenDePagoBLL.getSucursalBancoAdociado();
            return View();
        }
        public IActionResult guardarOrdenPago(string IdOrden , string IdBancoSucursal)
        {
            var idBanco = Convert.ToInt32(IdBancoSucursal.Split("-")[0]);
            var idSucursal = Convert.ToInt32(IdBancoSucursal.Split("-")[1]);
            _OrdenDePagoBLL.guardarOrdenDePagoTransaccion(Convert.ToInt32(IdOrden), idBanco, idSucursal); 
            return RedirectToAction("formPagarOrdenPagoView", "OrdenDePago");
        }
    }
}
