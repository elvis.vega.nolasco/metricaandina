﻿using AppWeb.Attributes;
using Bussines.Banco;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppWeb.Controllers
{
    [Autorizar("Operador1", "Administrador")]
    public class BancoController : Controller
    {
        private readonly IBancoBLL _BancoBll;

      
        public BancoController(IBancoBLL BancoBll)
        {
            this._BancoBll = BancoBll;
        }
        //[AllowAnonymous]
        //[Authorize(Roles = "Operador1")]

        
        public IActionResult Index()
        {
            return View("~/Views/Banco/listarBancos.cshtml", _BancoBll.getListaBancos());
        }
        //[HttpGet("/Banco/Editar/{id}")]
        public IActionResult FormEditar(int id)
        {
            Entidades.Banco objBanco;
            if (id > 0)
            {
                objBanco = _BancoBll.getBancoxId(id);
            }
            else
            {
                objBanco = new Entidades.Banco(){ Id = id };
            }
            
            return View("~/Views/Banco/_form_Banco.cshtml", objBanco);
        }
        public IActionResult Editar(Entidades.Banco objBanco)
        {
            if (!ModelState .IsValid)
            {
                return View("~/Views/Banco/_form_Banco.cshtml", objBanco);
            }
            if (objBanco.Id == 0)
            {
                _BancoBll.guardarBanco(objBanco);
            }
            else {
                _BancoBll.editBanco(objBanco);
            }
           
           return RedirectToAction("Index", "Banco");
        }
        public IActionResult Eliminar(int id)
        {
            _BancoBll.eliminarBanco(id);
            return RedirectToAction ("Index", "Banco");
        }

    }
}
