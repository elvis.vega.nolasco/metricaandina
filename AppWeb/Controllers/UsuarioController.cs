﻿using AppWeb.Models;
using Entidades;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using Bussines.Acceso;

namespace AppWeb.Controllers
{
    public class UsuarioController : Controller
    {
      
        public IConfiguration _configuration { get; }

        private readonly IAccesoBLL _AccesoBLL;

        public UsuarioController(IConfiguration configuration, IAccesoBLL AccesoBLL)
        {
            _configuration = configuration;
            _AccesoBLL = AccesoBLL;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult iniciarSession(UsuarioRQ user)
        {
            if (!ModelState.IsValid )
            {
                return View("~/Views/Home/Login.cshtml", user);
            }
            if (_AccesoBLL.ValidaUsuario(user.Usuario,user.Password,user.Rol))
            {
                HttpContext.Session.SetString("CUENTA", JsonConvert.SerializeObject(user));
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Login", "Home");

        }
       
    }
}
