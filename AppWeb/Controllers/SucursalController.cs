﻿using AppWeb.Attributes;
using Bussines.Sucursal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppWeb.Controllers
{
    [Autorizar("Operador1", "Administrador")]
    public class SucursalController : Controller
    {
        private readonly ISucursalBLL _SucursalBLL;

        public SucursalController(ISucursalBLL BancoBll)
        {
            this._SucursalBLL = BancoBll;
        }
        // GET: SucursalController
        public IActionResult Index()
        {
            return View("~/Views/Sucursal/listarSucursal.cshtml", _SucursalBLL.getListaSucursal());
        }
        //[HttpGet("/Banco/Editar/{id}")]
        public IActionResult FormEditar(int id)
        {
            Entidades.Sucursal objSucursal;
            if (id > 0)
            {
                objSucursal = _SucursalBLL.getSucursalxId(id);
            }
            else
            {
                objSucursal = new Entidades.Sucursal() { Id = id };
            }

            return View("~/Views/Sucursal/_form_Sucursal.cshtml", objSucursal);
        }
        public IActionResult Editar(Entidades.Sucursal objSucursal)
        {
            if (!ModelState.IsValid)
            {
                return View("~/Views/Sucursal/_form_Sucursal.cshtml", objSucursal);
            }
            if (objSucursal.Id == 0)
            {
                _SucursalBLL.guardarSucursal(objSucursal);
            }
            else
            {
                _SucursalBLL.editSucursal(objSucursal);
            }

            return RedirectToAction("Index", "Sucursal");
        }
        public IActionResult Eliminar(int id)
        {
            _SucursalBLL.eliminarSucursal(id);
            return RedirectToAction("Index", "Sucursal");
        }

    }
}
