﻿using AppWeb.Attributes;
using Bussines.Banco;
using Bussines.Sucursal;
using Entidades;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppWeb.Controllers
{
    [Autorizar("Operador1", "Administrador")]
    public class SucursalBancoController : Controller
    {
        private readonly IBancoBLL _BancoBLL;
        private readonly ISucursalBLL _SucursalBLL;
       // private readonly IHttpContextAccessor _httpContextAccessor;

        public SucursalBancoController(IBancoBLL BancoBLL, ISucursalBLL SucursalBLL)
        {
            this._BancoBLL = BancoBLL;
            this._SucursalBLL = SucursalBLL;
         //   this._httpContextAccessor = httpContextAccessor;
           // _httpContextAccessor.HttpContext.Session.SetString("SessionVar", "Prueba!");
        }
        public async Task<IActionResult> Index()
        {
            var lstBanco = new List<Banco>();
            lstBanco = _BancoBLL.getListaBancos();
            await Task.Run(() => Parallel.ForEach(lstBanco, (line, state, index) =>
            {
                lstBanco[(int)index].lstSucursal = _SucursalBLL.getListaSucursalPorIdBanco(lstBanco[(int)index].Id);
                lstBanco[(int)index].Sucursalesdisponibles = _SucursalBLL.getListaSucursalDisponiblesParaBanco(lstBanco[(int)index].Id);
            }));
            
            return View("~/Views/SucursalBanco/listarSucursalesPorBanco.cshtml", lstBanco);
        }
        /**Mostramos formulario donde se realizara la vinculacion del banco a diferentes sucursales
        */
        public IActionResult FormAsignarSucursalABanco(int idBanco)
        {
            var objBanco = _BancoBLL.getBancoxId(idBanco);
            objBanco.Sucursalesdisponibles = _SucursalBLL.getListaSucursalDisponiblesParaBanco(idBanco);

            return View("~/Views/SucursalBanco/_form_BancoSucursal.cshtml", objBanco);
        }
        public IActionResult AsociarHotelASucursal(Banco objbanco)
        {
            _SucursalBLL.asociarBancoSucursal(objbanco);
            return RedirectToAction("Index");
        }

        //EliminarSucursalDeBanco
        public IActionResult EliminarSucursalDeBanco(int idBanco, int idSucursal)
        {
            _SucursalBLL.EliminarSucursalDeBanco(idBanco, idSucursal);
            return RedirectToAction("Index");
        }
    }
}
