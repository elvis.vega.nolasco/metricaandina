﻿using AppWeb.Attributes.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AppWeb.Attributes
{
    public class AutorizarAttribute : ActionFilterAttribute, IAuthorizationFilter
    {
        private string[] perfiles;

        public AutorizarAttribute(params string[] perfiles)
        {
            this.perfiles = perfiles;
        }
        public void OnAuthorization(AuthorizationFilterContext filterContext)
        {
            if (!AutorizarUtil.Autorizar(filterContext.HttpContext.Session, perfiles))
            {
                var code = (int)HttpStatusCode.Forbidden;
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                        { "Controller", "Home" },
                        { "Action", "Autorization" },
                        { "e", code }
                    });
                filterContext.HttpContext.Response.StatusCode = code;
            }
        }
    }
}
