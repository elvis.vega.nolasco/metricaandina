﻿using AppWeb.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AppWeb.Attributes.Util
{
    public static class AutorizarUtil
    {
        public static bool Autorizar(ISession session, string[] perfiles)
        {
            var cuenta = JsonConvert.DeserializeObject<UsuarioRQ>(session.GetString("CUENTA"));
            return perfiles.Contains(cuenta.Rol);
        }
    }
}
