﻿using AccesoDatos.OrdenPago;
using Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bussines.OrdenPago
{
    public class OrdenDePagoBLL: IOrdenDePagoBLL
    {
        private readonly IOrdenPagoDAO _OrdenDePagoDAO;
        public OrdenDePagoBLL(IOrdenPagoDAO BancoDAO)
        {
            this._OrdenDePagoDAO = BancoDAO;
        }

        public List<Entidades.OrdenDePago> getListaOrdenDePago()
        {
            return _OrdenDePagoDAO._getListaOrdenDePago();

        }
        public List<Entidades.OrdenDePagoTransac> OrdenDePagoTransac()
        {
            return _OrdenDePagoDAO._OrdenDePagoTransac();

        }
        public List<Entidades.OrdenDePago> getListaOrdenDePagoPendientes()
        {
            return _OrdenDePagoDAO._getListaOrdenDePagoPendientes();

        }

        public async Task<List<Entidades.OrdenDePagoTransac>> getOrdenDePagoxSucursal(string tipoMoneda)
        {
            return await _OrdenDePagoDAO._getOrdenDePagoxSucursal(tipoMoneda);
        }
        public Entidades.OrdenDePago getOrdenDePagoxId(int id)
        {
            return _OrdenDePagoDAO._getOrdenDePagoxId(id);
        }

        public void editOrdenDePago(Entidades.OrdenDePago objOrdenDePago)
        {

            _OrdenDePagoDAO._editOrdenDePago(objOrdenDePago);
        }
        public void guardarOrdenDePagoTransaccion(int IdOrden, int idBanco, int idSucursal)
        {

            _OrdenDePagoDAO._guardarOrdenDePagoTransaccion(IdOrden, idBanco, idSucursal);
        }
        public void guardarOrdenDePago(Entidades.OrdenDePago objOrdenDePago)
        {

            _OrdenDePagoDAO._guardarOrdenDePago(objOrdenDePago);
        }

        public void eliminarOrdenDePago(int id)
        {

            _OrdenDePagoDAO._eliminarOrdenDePago(id);
        }

        public List<SucursalBanco> getSucursalBancoAdociado()
        {
            return _OrdenDePagoDAO._getSucursalBancoAdociado();
        }
    }
}
