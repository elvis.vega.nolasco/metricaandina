﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bussines.OrdenPago
{
   public  interface IOrdenDePagoBLL
    {
        List<Entidades.OrdenDePago> getListaOrdenDePago();
        List<Entidades.OrdenDePago> getListaOrdenDePagoPendientes();
        
        Entidades.OrdenDePago getOrdenDePagoxId(int Id);
        void editOrdenDePago(Entidades.OrdenDePago objOrdenDePago);
        void eliminarOrdenDePago(int id);
        void guardarOrdenDePago(Entidades.OrdenDePago objOrdenDePago);
        List<Entidades.SucursalBanco> getSucursalBancoAdociado();

        List<Entidades.OrdenDePagoTransac> OrdenDePagoTransac();
        void guardarOrdenDePagoTransaccion(int IdOrden, int idBanco, int idSucursal);

        Task<List<Entidades.OrdenDePagoTransac>> getOrdenDePagoxSucursal(string tipoMoneda);
   }
}
