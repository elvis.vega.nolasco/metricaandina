﻿using AccesoDatos.Acceso;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bussines.Acceso
{
    public class AccesoBLL : IAccesoBLL
    {
        private readonly IAccesoDAO _accesoDAO;

        public AccesoBLL(IAccesoDAO AccesoDAO) {
            this._accesoDAO = AccesoDAO;
        }
        public bool ValidaUsuario(string user, string pasword, string Rol)
        {
            return _accesoDAO._ValidaUsuario(user,pasword,Rol);
        }
    }
}
