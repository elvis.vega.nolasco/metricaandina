﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bussines.Sucursal
{
   public  interface ISucursalBLL
    {
        List<Entidades.Sucursal> getListaSucursal();
        Entidades.Sucursal getSucursalxId(int Id);
        void editSucursal(Entidades.Sucursal objBanco);
        void eliminarSucursal(int id);
        void guardarSucursal(Entidades.Sucursal objBanco);

        List<Entidades.Sucursal> getListaSucursalPorIdBanco(int idBanco);
        List<Entidades.Sucursal> getListaSucursalDisponiblesParaBanco(int idBanco);
        void asociarBancoSucursal(Entidades.Banco objBanco);
        void EliminarSucursalDeBanco(int idBanco, int idSucursal);
    }
}
