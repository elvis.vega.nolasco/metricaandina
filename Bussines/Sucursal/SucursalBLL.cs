﻿using AccesoDatos.Sucursal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bussines.Sucursal
{
    public class SucursalBLL : ISucursalBLL
    {
        private readonly ISucursalDAO _SucursalDAO;
        public SucursalBLL(ISucursalDAO BancoDAO)
        {
            this._SucursalDAO = BancoDAO;
        }

        public List<Entidades.Sucursal> getListaSucursal()
        {
            return _SucursalDAO._getListaSucursal();

        }

        public Entidades.Sucursal getSucursalxId(int id)
        {
            return _SucursalDAO._getSucursalxId(id);
        }

        public void editSucursal(Entidades.Sucursal objBanco)
        {

            _SucursalDAO._editSucursal(objBanco);
        }
        public void guardarSucursal(Entidades.Sucursal objBanco)
        {

            _SucursalDAO._guardarSucursal(objBanco);
        }

        public void eliminarSucursal(int id)
        {

            _SucursalDAO._eliminarSucursal(id);
        }

        public List<Entidades.Sucursal> getListaSucursalPorIdBanco(int idBanco) {
            return _SucursalDAO._getListaSucursalPorIdBanco(idBanco);
        }
        public List<Entidades.Sucursal> getListaSucursalDisponiblesParaBanco(int idBanco)
        {
            return _SucursalDAO._getListaSucursalDisponiblesParaBanco(idBanco);
        }
        public void asociarBancoSucursal(Entidades.Banco objBanco)
        {
            _SucursalDAO._asociarBancoSucursal(objBanco.Id, objBanco.IdSucursal);
        }
        public void EliminarSucursalDeBanco(int idBanco, int idSucursal) {
            _SucursalDAO._EliminarSucursalDeBanco(idBanco, idSucursal);
        }
    }
}
