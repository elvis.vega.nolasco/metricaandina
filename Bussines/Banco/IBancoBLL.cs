﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace Bussines.Banco
{
    public  interface IBancoBLL
    {
      List<Entidades.Banco> getListaBancos ();
      Entidades.Banco getBancoxId(int Id);
      void editBanco(Entidades.Banco objBanco);
        void eliminarBanco(int id);
        void guardarBanco(Entidades.Banco objBanco);

      
    }
}
