﻿using AccesoDatos.Banco;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bussines.Banco
{
    public class BancoBLL : IBancoBLL
    {
        private readonly IBancoDAO _BancoDAO;
        public BancoBLL( IBancoDAO BancoDAO)
        {
            this._BancoDAO = BancoDAO;
        }

        public List<Entidades.Banco> getListaBancos()
        {
            return  _BancoDAO._getListaBancos();

        }

        public Entidades.Banco getBancoxId(int id)
        {
            return _BancoDAO._getBancoxId(id);
        }

        public void editBanco(Entidades.Banco objBanco)
        {

             _BancoDAO._editBanco(objBanco);
        }
        public void guardarBanco(Entidades.Banco objBanco)
        {

            _BancoDAO._guardarBanco(objBanco);
        }
        
        public void eliminarBanco(int id)
        {

            _BancoDAO._eliminarBanco(id);
        }

     
    }
}
