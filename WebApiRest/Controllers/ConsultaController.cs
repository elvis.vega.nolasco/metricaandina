﻿using Bussines.OrdenPago;
using Bussines.Sucursal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiRest.Controllers
{
    [Route("api/Consulta")]
    [ApiController]
    public class ConsultaController : ControllerBase
    {
        private readonly IOrdenDePagoBLL _ordenDePago;
        private readonly ISucursalBLL _sucursalBLL;

        public ConsultaController(IOrdenDePagoBLL ordenDePago, ISucursalBLL sucursalBLL)
        {
            this._ordenDePago = ordenDePago;
            this._sucursalBLL = sucursalBLL;
        }
        /**Valores de Entrada S/D S= SOLES D=DOLAR
         * http://localhost:58076/api/Consulta/GetOrdenPagoSucursal/S
         */
        [HttpGet("GetOrdenPagoSucursal/{tipoMoneda}")]
        public async Task<ActionResult<List<Entidades.OrdenDePagoTransac>>> GetOrdenPagoSucursal(string tipoMoneda)
        {
            var ordenesDePago = await _ordenDePago.getOrdenDePagoxSucursal(tipoMoneda.ToUpper());
            if (ordenesDePago.Count() == 0)
            {
                return NotFound();
            }
            return Ok(ordenesDePago);
        }
        /**
         * http://localhost:58076/api/consulta/GetSucursalPorBanco/5
         */
        [HttpGet("GetSucursalPorBanco/{idBanco}")]
        public ActionResult<List<Entidades.Sucursal>> GetSucursalPorBanco(int idBanco)
        {
            var ordenesDePago = _sucursalBLL.getListaSucursalPorIdBanco(idBanco);
            if (ordenesDePago.Count() == 0)
            {
                return NotFound();
            }
            return Ok(ordenesDePago);
        }
    }
}
