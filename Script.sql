USE [master]
GO
/****** Object:  Database [db_banco]   Script Date: 12/06/2020 01:05:55 ******/
CREATE DATABASE [db_banco]
GO
ALTER DATABASE [db_banco]SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_banco].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_banco]SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [db_banco]SET ANSI_NULLS OFF
GO
ALTER DATABASE [db_banco]SET ANSI_PADDING OFF
GO
ALTER DATABASE [db_banco]SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [db_banco]SET ARITHABORT OFF
GO
ALTER DATABASE [db_banco]SET AUTO_CLOSE ON
GO
ALTER DATABASE [db_banco]SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [db_banco]SET AUTO_SHRINK OFF
GO
ALTER DATABASE [db_banco]SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [db_banco]SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [db_banco]SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [db_banco]SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [db_banco]SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [db_banco]SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [db_banco]SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [db_banco]SET  DISABLE_BROKER
GO
ALTER DATABASE [db_banco]SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [db_banco]SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [db_banco]SET TRUSTWORTHY OFF
GO
ALTER DATABASE [db_banco]SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [db_banco]SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [db_banco]SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [db_banco]SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [db_banco]SET  READ_WRITE
GO
ALTER DATABASE [db_banco]SET RECOVERY SIMPLE
GO
ALTER DATABASE [db_banco]SET  MULTI_USER
GO
ALTER DATABASE [db_banco]SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [db_banco]SET DB_CHAINING OFF
GO
USE [db_banco]
GO
/****** Object:  Table [dbo].[Banco]    Script Date: 12/06/2020 01:05:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Banco](
	[id_banco] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](80) NOT NULL,
	[direccion] [varchar](100) NOT NULL,
	[fecha_registro] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_banco] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Banco] ON
INSERT [dbo].[Banco] ([id_banco], [nombre], [direccion], [fecha_registro]) VALUES (2, N'diners', N'miraflores', CAST(0x0000AC88003239A9 AS DateTime))
INSERT [dbo].[Banco] ([id_banco], [nombre], [direccion], [fecha_registro]) VALUES (3, N'BBVA', N'LIMA', CAST(0x0000AC8800686246 AS DateTime))
INSERT [dbo].[Banco] ([id_banco], [nombre], [direccion], [fecha_registro]) VALUES (4, N'PICHINCHA', N'CHINCHA', CAST(0x0000AC88006870B6 AS DateTime))
INSERT [dbo].[Banco] ([id_banco], [nombre], [direccion], [fecha_registro]) VALUES (5, N'CAJA SULLA', N'DIRECCION', CAST(0x0000AC88006887FA AS DateTime))
SET IDENTITY_INSERT [dbo].[Banco] OFF
/****** Object:  Table [dbo].[Rol]    Script Date: 12/06/2020 01:05:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rol](
	[idRol] [int] IDENTITY(1,1) NOT NULL,
	[operador1] [varchar](80) NOT NULL,
	[operador2] [varchar](80) NOT NULL,
	[adminitrador] [varchar](80) NOT NULL,
	[estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrdenesPago]    Script Date: 12/06/2020 01:05:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrdenesPago](
	[id_ordenpago] [int] IDENTITY(1,1) NOT NULL,
	[monto] [decimal](18, 2) NOT NULL,
	[moneda] [varchar](80) NOT NULL,
	[estado] [varchar](80) NOT NULL,
	[fecha_registro] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_ordenpago] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[OrdenesPago] ON
INSERT [dbo].[OrdenesPago] ([id_ordenpago], [monto], [moneda], [estado], [fecha_registro]) VALUES (2, CAST(1 AS Decimal(18, 0)), N'Dolares', N'Pagado', CAST(0x0000AC88007C1E40 AS DateTime))
INSERT [dbo].[OrdenesPago] ([id_ordenpago], [monto], [moneda], [estado], [fecha_registro]) VALUES (3, CAST(2999 AS Decimal(18, 0)), N'Dolares', N'Pagado', CAST(0x0000AC88007DD26A AS DateTime))
SET IDENTITY_INSERT [dbo].[OrdenesPago] OFF
/****** Object:  Table [dbo].[Moneda]    Script Date: 12/06/2020 01:05:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Moneda](
	[idMoneda] [int] IDENTITY(1,1) NOT NULL,
	[Moneda] [varchar](80) NOT NULL,
	[fechaRegistro] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[idMoneda] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 12/06/2020 01:05:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[usuario] [varchar](80) NOT NULL,
	[clave] [varchar](80) NOT NULL,
	[fechaRegistro] [datetime] NULL,
	[estado] [int] NOT NULL,
	[Rol] [varchar](80) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON
INSERT [dbo].[Usuario] ([id], [usuario], [clave], [fechaRegistro], [estado], [Rol]) VALUES (4, N'admin', N'123', CAST(0x0000AC8801043EA1 AS DateTime), 1, N'Administrador')
INSERT [dbo].[Usuario] ([id], [usuario], [clave], [fechaRegistro], [estado], [Rol]) VALUES (5, N'ope1', N'123', CAST(0x0000AC8801043EA1 AS DateTime), 1, N'Operador1')
INSERT [dbo].[Usuario] ([id], [usuario], [clave], [fechaRegistro], [estado], [Rol]) VALUES (6, N'ope2', N'123', CAST(0x0000AC8801043EA1 AS DateTime), 1, N'Operador2')
SET IDENTITY_INSERT [dbo].[Usuario] OFF
/****** Object:  StoredProcedure [dbo].[validarCuenta]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[validarCuenta](
@p_user varchar(80),
@p_pasword varchar(80),
@p_rol varchar(80)
)
as
begin
select * from dbo.Usuario
where usuario = @p_user
and clave = @p_pasword
and Rol = @p_rol
	
end;
GO
/****** Object:  Table [dbo].[Sucursales]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sucursales](
	[id_sucursal] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](80) NOT NULL,
	[direccion] [varchar](100) NOT NULL,
	[fecha_registro] [datetime] NULL,
	[id_banco] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_sucursal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Sucursales] ON
INSERT [dbo].[Sucursales] ([id_sucursal], [nombre], [direccion], [fecha_registro], [id_banco]) VALUES (2, N'Magdalena ', N'Av Victor Raul', CAST(0x0000AC88004217CE AS DateTime), NULL)
INSERT [dbo].[Sucursales] ([id_sucursal], [nombre], [direccion], [fecha_registro], [id_banco]) VALUES (3, N'MIRAFLORES', N'MIRAFLORES', CAST(0x0000AC880068997E AS DateTime), NULL)
INSERT [dbo].[Sucursales] ([id_sucursal], [nombre], [direccion], [fecha_registro], [id_banco]) VALUES (4, N'ICA', N'ICA', CAST(0x0000AC880068A41E AS DateTime), NULL)
INSERT [dbo].[Sucursales] ([id_sucursal], [nombre], [direccion], [fecha_registro], [id_banco]) VALUES (5, N'LOS OLIVOS', N'LOS OLIVOS', CAST(0x0000AC880068B11A AS DateTime), NULL)
INSERT [dbo].[Sucursales] ([id_sucursal], [nombre], [direccion], [fecha_registro], [id_banco]) VALUES (6, N'INDEPENDEN', N'INDEPENDE', CAST(0x0000AC880068BD43 AS DateTime), NULL)
INSERT [dbo].[Sucursales] ([id_sucursal], [nombre], [direccion], [fecha_registro], [id_banco]) VALUES (7, N'ATE', N'ATE', CAST(0x0000AC880068C4E7 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Sucursales] OFF
/****** Object:  StoredProcedure [dbo].[listar_OrdenDePagoPending]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[listar_OrdenDePagoPending]
as
begin

	select *  from dbo.OrdenesPago 
	where estado <> 'Pagado'
	order by fecha_registro desc
end;
GO
/****** Object:  StoredProcedure [dbo].[listar_OrdenDePago]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[listar_OrdenDePago]
as
begin

	select *  from dbo.OrdenesPago 
	order by fecha_registro desc
end;
GO
/****** Object:  StoredProcedure [dbo].[listar_Bancos]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[listar_Bancos]
as
begin

	select *  from dbo.Banco 
	order by fecha_registro desc
end;
GO
/****** Object:  StoredProcedure [dbo].[update_OrdenesPago]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[update_OrdenesPago](
	@p_id_ordenpago int,
	@p_monto decimal,
	@p_moneda varchar(100),
	@p_estado varchar(80)
)
as
begin
	update dbo.OrdenesPago set
	monto = @p_monto,
	moneda = @p_moneda,
	estado =@p_estado 
	where id_ordenpago = @p_id_ordenpago;
end;
GO
/****** Object:  StoredProcedure [dbo].[update_banco]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[update_banco](
	@p_id_banco int,
	@p_nombre varchar(80),
	@p_direccion varchar(100)
)
as
begin
	update dbo.Banco set
	nombre = @p_nombre,
	direccion = @p_direccion
	where id_banco = @p_id_banco;
end;
GO
/****** Object:  StoredProcedure [dbo].[insert_OrdenesPago]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[insert_OrdenesPago](
	@p_monto decimal,
	@p_moneda varchar(100),
	@p_estado varchar(80)
)
as
begin
	insert into dbo.OrdenesPago(monto, moneda, estado)values(@p_monto,@p_moneda,@p_estado);
end;
GO
/****** Object:  StoredProcedure [dbo].[get_OrdenDePagoxId]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[get_OrdenDePagoxId](
@p_id_OrdenDePago int
)
as
begin

	select *  from dbo.OrdenesPago 
	where id_ordenpago= @p_id_OrdenDePago
	order by fecha_registro desc
end;
GO
/****** Object:  StoredProcedure [dbo].[get_BancosxId]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[get_BancosxId](
@p_id_banco int
)
as
begin
	select *  from dbo.Banco 
	where id_banco = @p_id_banco
	order by fecha_registro desc
end;
GO
/****** Object:  StoredProcedure [dbo].[insert_banco]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[insert_banco](
	@p_nombre varchar(80),
	@p_direccion varchar(100)
)
as
begin
	insert into dbo.Banco(nombre, direccion)values(@p_nombre,@p_direccion);
end;
GO
/****** Object:  StoredProcedure [dbo].[delete_OrdenesPago]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[delete_OrdenesPago](
	@p_id_ordenpago int
)
as
begin
	delete from dbo.OrdenesPago 
	where id_ordenpago=@p_id_ordenpago;
end;
GO
/****** Object:  StoredProcedure [dbo].[delete_banco]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[delete_banco](
	@p_id_banco int
)
as
begin
	delete from dbo.Banco where id_banco=@p_id_banco;
end;
GO
/****** Object:  Table [dbo].[BancoSucursal]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BancoSucursal](
	[id_bancosucursal] [int] IDENTITY(1,1) NOT NULL,
	[id_banco] [int] NULL,
	[id_sucursal] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_bancosucursal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[BancoSucursal] ON
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (3, 2, 6)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (7, 3, 3)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (8, 4, 7)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (9, 3, 7)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (10, 4, 6)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (11, 4, 5)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (13, 2, 7)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (14, 2, 5)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (15, 2, 4)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (16, 3, 6)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (17, 2, 3)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (18, 2, 2)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (19, 5, 7)
INSERT [dbo].[BancoSucursal] ([id_bancosucursal], [id_banco], [id_sucursal]) VALUES (20, 5, 6)
SET IDENTITY_INSERT [dbo].[BancoSucursal] OFF
/****** Object:  StoredProcedure [dbo].[get_SucursalxId]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[get_SucursalxId](
@p_id_sucursal int
)
as
begin

	select *  from dbo.Sucursales 
	where id_sucursal = @p_id_sucursal
	order by fecha_registro desc
end;
GO
/****** Object:  StoredProcedure [dbo].[insert_sucursal]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[insert_sucursal](
	@p_nombre varchar(80),
	@p_direccion varchar(100)
	--@p_id_banco int,
	--@p_id_ordenpago int
)
as
	declare @id_sucursal int;
begin
	insert into dbo.Sucursales(nombre, direccion)values(@p_nombre,@p_direccion);
	--set @id_sucursal = (select max(id_sucursal) from Sucursales);
	--insert into dbo.Sucursales_OrdenesPago(id_sucursal,id_ordenpago)values(@id_sucursal, @p_id_ordenpago);
end;
GO
/****** Object:  Table [dbo].[Sucursales_OrdenesPago]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sucursales_OrdenesPago](
	[id_sucur_orden] [int] IDENTITY(1,1) NOT NULL,
	[id_banco] [int] NULL,
	[id_sucursal] [int] NULL,
	[id_ordenpago] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_sucur_orden] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Sucursales_OrdenesPago] ON
INSERT [dbo].[Sucursales_OrdenesPago] ([id_sucur_orden], [id_banco], [id_sucursal], [id_ordenpago]) VALUES (9, 2, 6, 3)
INSERT [dbo].[Sucursales_OrdenesPago] ([id_sucur_orden], [id_banco], [id_sucursal], [id_ordenpago]) VALUES (10, 4, 7, 2)
SET IDENTITY_INSERT [dbo].[Sucursales_OrdenesPago] OFF
/****** Object:  StoredProcedure [dbo].[delete_sucursal]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[delete_sucursal](
	@p_id_sucursal int
)
as
begin
	delete from dbo.Sucursales where id_sucursal=@p_id_sucursal;
end;
GO
/****** Object:  StoredProcedure [dbo].[update_sucursal]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[update_sucursal](
	@p_id_sucursal int,
	@p_nombre varchar(80),
	@p_direccion varchar(100)
	--@p_id_banco int
)
as
begin
	update dbo.Sucursales set
	nombre = @p_nombre,
	direccion = @p_direccion
	--id_banco = @p_id_banco
	where id_sucursal = @p_id_sucursal;
end;
GO
/****** Object:  StoredProcedure [dbo].[listar_Sucursal]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[listar_Sucursal]
as
begin

	select *  from dbo.Sucursales 
	order by fecha_registro desc
end;
GO
/****** Object:  StoredProcedure [dbo].[listar_OrdenDePagotransac]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[listar_OrdenDePagotransac]
as
begin
select sop.id_ordenpago,o.monto,o.moneda, sop.id_banco, sop.id_sucursal, b.nombre + ' - ' + s.nombre as descripcion from dbo.Sucursales_OrdenesPago sop
inner join dbo.Banco b on b.id_banco = sop.id_banco
inner join dbo.Sucursales s on s.id_sucursal= sop.id_sucursal
inner join dbo.OrdenesPago o on o.id_ordenpago= sop.id_ordenpago
order by sop.id_sucur_orden desc

end;
GO
/****** Object:  StoredProcedure [dbo].[listar_OrdenDePagoSucursal]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[listar_OrdenDePagoSucursal](
@p_in_moneda varchar(1)
)
as
begin
select sop.id_ordenpago,o.monto,o.moneda, sop.id_banco, sop.id_sucursal, s.nombre as descripcion from dbo.Sucursales_OrdenesPago sop
inner join dbo.Sucursales s on s.id_sucursal= sop.id_sucursal
inner join dbo.OrdenesPago o on o.id_ordenpago= sop.id_ordenpago
where (o.moneda = case when @p_in_moneda = 'S' then 'Soles'
					  when @p_in_moneda = 'D' then 'Dolares'
					  end
	  or @p_in_moneda is null)					  
order by sop.id_sucur_orden desc

end;
GO
/****** Object:  StoredProcedure [dbo].[insert_Sucursales_OrdenesPago]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[insert_Sucursales_OrdenesPago](
@p_id_banco int,
@p_id_sucursal int,
@p_id_orden_pago int
)
as
begin

insert into dbo.Sucursales_OrdenesPago(id_banco, id_sucursal, id_ordenpago)values(@p_id_banco,@p_id_sucursal, @p_id_orden_pago)
end;
GO
/****** Object:  StoredProcedure [dbo].[listar_SucursalPorIdBanco]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[listar_SucursalPorIdBanco](
@p_id_banco int
)
as
begin

	select s.nombre,s.direccion,s.id_sucursal, s.fecha_registro   from dbo.BancoSucursal bs
	--inner join dbo.Banco b on b.id_banco = bs.id_banco
	inner join dbo.Sucursales s on s.id_sucursal = bs.id_sucursal
	where bs.id_banco =@p_id_banco 
	order by s.fecha_registro desc
end;
GO
/****** Object:  StoredProcedure [dbo].[listar_SucursalDispoPorIdBanco]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[listar_SucursalDispoPorIdBanco](
@p_id_banco int
)
as
begin

select suc.nombre,suc.direccion,suc.id_sucursal, suc.fecha_registro from dbo.Sucursales suc
where suc.id_sucursal not in (
select s.id_sucursal  from dbo.BancoSucursal bs
	inner join dbo.Sucursales s on s.id_sucursal = bs.id_sucursal
	where bs.id_banco =@p_id_banco)
order by suc.fecha_registro desc
	
end;
GO
/****** Object:  StoredProcedure [dbo].[insert_OrdenesPagoTransac]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[insert_OrdenesPagoTransac](
	@p_id_orden decimal,
	@p_id_banco varchar(100),
	@p_id_sucursal varchar(80)
)
as
begin
	insert into dbo.Sucursales_OrdenesPago(id_banco, id_sucursal, id_ordenpago)values(@p_id_banco,@p_id_sucursal,@p_id_orden);
	update dbo.OrdenesPago
	set estado= 'Pagado'
	where id_ordenpago= @p_id_orden;
end;
GO
/****** Object:  StoredProcedure [dbo].[insert_BancoSucursal]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[insert_BancoSucursal](
	@p_id_banco varchar(80),
	@p_id_sucursal varchar(100)
	--@p_id_banco int,
	--@p_id_ordenpago int
)
as
	declare @id_sucursal int;
begin
	insert into dbo.BancoSucursal(id_banco, id_sucursal)values(@p_id_banco,@p_id_sucursal);
	--set @id_sucursal = (select max(id_sucursal) from Sucursales);
	--insert into dbo.Sucursales_OrdenesPago(id_sucursal,id_ordenpago)values(@id_sucursal, @p_id_ordenpago);
end;
GO
/****** Object:  StoredProcedure [dbo].[get_Sucursales_banco]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[get_Sucursales_banco]
as
begin
select b.id_banco, s.id_sucursal, b.nombre + ' - ' +s.nombre as bancoSucursal  from dbo.BancoSucursal bs
inner join dbo.Banco b on b.id_banco = bs.id_banco
inner join dbo.Sucursales s on s.id_sucursal = bs.id_sucursal

end;
GO
/****** Object:  StoredProcedure [dbo].[delete_sucursaldebanco]    Script Date: 12/06/2020 01:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[delete_sucursaldebanco](
	@p_id_banco int,
	@p_id_sucursal int
)
as
begin
	delete from dbo.BancoSucursal where id_sucursal=@p_id_sucursal and id_banco= @p_id_banco;
end;
GO
/****** Object:  Default [DF__Banco__fecha_reg__0AD2A005]    Script Date: 12/06/2020 01:05:56 ******/
ALTER TABLE [dbo].[Banco] ADD  DEFAULT (getdate()) FOR [fecha_registro]
GO
/****** Object:  Default [DF__OrdenesPa__fecha__4CA06362]    Script Date: 12/06/2020 01:05:56 ******/
ALTER TABLE [dbo].[OrdenesPago] ADD  DEFAULT (getdate()) FOR [fecha_registro]
GO
/****** Object:  Default [DF__Moneda__fechaReg__46E78A0C]    Script Date: 12/06/2020 01:05:56 ******/
ALTER TABLE [dbo].[Moneda] ADD  DEFAULT (getdate()) FOR [fechaRegistro]
GO
/****** Object:  Default [DF__Usuario__fechaRe__5629CD9C]    Script Date: 12/06/2020 01:05:56 ******/
ALTER TABLE [dbo].[Usuario] ADD  DEFAULT (getdate()) FOR [fechaRegistro]
GO
/****** Object:  Default [DF__Sucursale__fecha__0F975522]    Script Date: 12/06/2020 01:05:57 ******/
ALTER TABLE [dbo].[Sucursales] ADD  DEFAULT (getdate()) FOR [fecha_registro]
GO
/****** Object:  ForeignKey [FK__Sucursale__id_ba__108B795B]    Script Date: 12/06/2020 01:05:57 ******/
ALTER TABLE [dbo].[Sucursales]  WITH CHECK ADD FOREIGN KEY([id_banco])
REFERENCES [dbo].[Banco] ([id_banco])
GO
/****** Object:  ForeignKey [FK__BancoSucu__id_ba__2D27B809]    Script Date: 12/06/2020 01:05:57 ******/
ALTER TABLE [dbo].[BancoSucursal]  WITH CHECK ADD FOREIGN KEY([id_banco])
REFERENCES [dbo].[Banco] ([id_banco])
GO
/****** Object:  ForeignKey [FK__BancoSucu__id_su__2E1BDC42]    Script Date: 12/06/2020 01:05:57 ******/
ALTER TABLE [dbo].[BancoSucursal]  WITH CHECK ADD FOREIGN KEY([id_sucursal])
REFERENCES [dbo].[Sucursales] ([id_sucursal])
GO
/****** Object:  ForeignKey [FK__Sucursale__id_ba__5BE2A6F2]    Script Date: 12/06/2020 01:05:57 ******/
ALTER TABLE [dbo].[Sucursales_OrdenesPago]  WITH CHECK ADD FOREIGN KEY([id_banco])
REFERENCES [dbo].[Banco] ([id_banco])
GO
/****** Object:  ForeignKey [FK__Sucursale__id_or__5DCAEF64]    Script Date: 12/06/2020 01:05:57 ******/
ALTER TABLE [dbo].[Sucursales_OrdenesPago]  WITH CHECK ADD FOREIGN KEY([id_ordenpago])
REFERENCES [dbo].[OrdenesPago] ([id_ordenpago])
GO
/****** Object:  ForeignKey [FK__Sucursale__id_su__5CD6CB2B]    Script Date: 12/06/2020 01:05:57 ******/
ALTER TABLE [dbo].[Sucursales_OrdenesPago]  WITH CHECK ADD FOREIGN KEY([id_sucursal])
REFERENCES [dbo].[Sucursales] ([id_sucursal])
GO
