﻿using System;

namespace Problema01
{
    class Program
    {
        static void Main(string[] args)
        {
            var strentrada = Convert.ToString(Console.ReadLine());
            ChangeString objChangeString = new ChangeString();
            var strSalida = objChangeString.Build(strentrada);
            Console.WriteLine("OutPut: {0}", strSalida);
            Console.ReadKey();
        }
    }
}
