﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Problema01
{
    public class ChangeString
    {
        public string Build(string entrada)
        {
            var varAbecedario = "abcdefghijklmnñopqrstuvwxyz";
            char[] ArrayString = entrada.ToCharArray();
            string varSalida = string.Empty;
            foreach (var element in ArrayString)
            {
                if (!char.IsNumber(element) && !string.IsNullOrEmpty(element.ToString().Trim() ))
                {
                    varAbecedario = char.IsUpper(element) ? varAbecedario.ToUpper() : varAbecedario.ToLower();
                    var intposicion = varAbecedario.IndexOf(element);
                    if (intposicion < varAbecedario.Length - 1)
                        varSalida += varAbecedario.Substring(intposicion + 1, 1);
                    else
                        varSalida += varAbecedario.Substring(0, 1);
                }
                else
                {
                    varSalida += element.ToString();
                }
            }
            return varSalida;
        }
    }
}
